=====
QHist
=====

.. image:: https://img.shields.io/pypi/v/qhist.svg
   :target: https://pypi.org/project/qhist
.. image:: https://gitlab.com/ckhurewa/qhist/badges/master/pipeline.svg
   :target: https://gitlab.com/ckhurewa/qhist/commits/master
.. image:: https://gitlab.com/ckhurewa/qhist/badges/master/coverage.svg
   :target: https://ckhurewa.gitlab.io/qhist
.. image:: https://img.shields.io/badge/License-GPL%20v3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0
.. image:: https://readthedocs.org/projects/qhist/badge/?version=latest
   :target: http://qhist.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status  
.. image:: https://img.shields.io/pypi/pyversions/qhist.svg

``QHist`` -- A Quick Histogram drawer for ``ROOT::TTree`` for smoother HEP analysis!

Examples:

- Simple draw from tree and parameter (branch name)::

    h1 = QHist()
    h1.trees = t1
    h1.params = 'M'
    h1.draw()

  .. image:: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/1.png
     :width: 160px
     :target: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/1.png 

- Overlaid comparison between branches, compact syntax::

    QHist(trees=t1, params=['mu_PT/1e3','pi_PT/1e3'], xlabel='PT [GeV]', xmax=60).draw()

  .. image:: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/2.png
     :width: 160px
     :target: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/2.png 

- Compare between trees, with filtering, and reusable via templating::

    H = QHist(trees=[t1, t2, t3], filters=['mu_PT>20e3'])
    H(params='APT'  , xmin=0  , xlabel='PT-asymmetry').draw()
    H(params='M/1e3', xmax=120, xlabel='Mass [GeV]').draw()

  .. class:: center
  .. image:: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/3a.png
     :width: 160px
     :target: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/3a.png 
  .. image:: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/3b.png
     :width: 160px
     :target: https://gitlab.com/ckhurewa/qhist/raw/master/docs/figs/3b.png

Read more on `<https://qhist.readthedocs.io>`_


Installation & dependencies
---------------------------

It's available from ``pip install qhist``. 
The package requires an existing installation of ``PyROOT``.

Disclaimer
----------

This packacge was written and used during my PhD in 2013-2017 at EPFL (Lausanne) and LHCb collaboration (CERN),
for the work in *Z->tau tau* cross-section measurement and *H->mu tau* searches at LHCb (8TeV).
I hope it can be of a good use for future analysis...

